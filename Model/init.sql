-- Efface la BDD si elle existe
DROP database if exists ContactGraph;
-- Créer la BDD
create database ContactGraph;
-- Utiliser  la BDD
use ContactGraph;

create table People(
    id int auto_increment NOT NULL Primary Key,
    name varchar(255) NOT NULL
);

create table Contact(
    people1 int not null,
    people2 int not null,
    FOREIGN KEY (people1) REFERENCES People(id) ON DELETE CASCADE,
    FOREIGN KEY (people2) REFERENCES People(id)  ON DELETE CASCADE
);

DROP USER IF EXISTS tousStopAntiCovid@'127.0.0.1';
CREATE USER tousStopAntiCovid@'127.0.0.1' identified by 'admin1234';
GRANT ALL privileges on ContactGraph.* TO tousStopAntiCovid@'127.0.0.1';
