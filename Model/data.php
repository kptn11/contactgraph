<?php
    include_once "debug.php";
//connection a la base de données
    $host = '127.0.0.1'; //adresse ip 
    $db   = 'ContactGraph'; //nom de la database
    $user = 'tousStopAntiCovid'; //nom de l'utilisateur
    $pass = 'admin1234'; //mot de pass
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass); //PHP data object

    function createPeople($name){
        global $pdo;
        $req = $pdo->prepare("insert into People (name) values(?)");
        $req->execute([$name]);
    };
    function readallPeople(){
        global $pdo;
        $req = $pdo->query("select * from People");
        return $req->fetchAll();
    };
    function readPeoplebyid($id){
        global $pdo;
        $req = $pdo->prepare("select * from People where id =?");
        $req ->execute([$id]);
        return $req -> fetchAll();
    };
    function updatePeople($id, $name){
        global $pdo;
        $req = $pdo->prepare("update People set name=? where id=?;");
        $req->execute([$name, $id]);
    };
    function deleteboby($id){
        global $pdo;
        $req = $pdo->prepare("delete from People where id=?;");
        $req->execute([$id]);
    };

    function createContact($people1, $people2){
        global $pdo;
        $req = $pdo->prepare("insert into Contact(people1,people2) values(?,?);");
        $req->execute([$people1, $people2]);
    };
    function readAllC(){
        global $pdo;
        $req = $pdo->query("SELECT * FROM Contact");
        return $req->fetchAll();
    }
    // function readAllContact(){
    //     global $pdo;
    //     $req = $pdo->query("select p.name from People p INNER JOIN Contact c ON c.name = p.name");
    //     return $req->fetchAll();
    // };

    function readContactbyid($id){
        global $pdo;
        $req = $pdo->prepare("select * from Contact where id =?");
        $req ->execute([$id]);
        return $req -> fetchAll();
    };
    function updateContact($id, $name){
        global $pdo;
        $req = $pdo->prepare("update Contact set name=? where id=?;");
        $req->execute([$name, $id]);
    };
    function deleteContact($id){
        global $pdo;
        $req = $pdo->prepare("delete from Contact where id=?;");
        $req->execute([$id]);
    };
    function deleteBdd(){
        global $pdo;
        $people = $pdo-> prepare("delete from People;");
        $req = $pdo-> prepare("delete from Contact;");
        $people->execute();
        $req->execute();
        return 'DELETE';
    };
    function readAllData(){
        global $pdo;
        $req = $pdo->query("SELECT id, name FROM People UNION SELECT * FROM Contact");
        return $req->fetchAll();
    }
    function readAllDataP(){
        global $pdo;
        $req = $pdo->query("SELECT
        p.id AS id,
        p.name AS name
    FROM
        People p
    ORDER BY
        p.id ASC;");
        return $req->fetchAll();
    }
    function readAllDataC(){
        global $pdo;
        $req = $pdo->query("SELECT
        c.people1 AS `from`,
        c.people2 AS `to`
    FROM
        Contact c
    ORDER BY
        c.people1 ASC, c.people2 ASC;");
        return $req->fetchAll();
    }
?>