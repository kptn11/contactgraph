
use ContactGraph;



INSERT INTO People(name) VALUES ('Sunshine'),
('Peppa Pig'),
('Red Velvet'),
('Chance'),
('Einstein'),
('Starbuck'),
('Amiga'),
('Flower'),
('PB&J'),
('Tough Guy'),
('Foxy'),
('Robin'),
('Cumulus'),
('Cheese'),
('Sourdough'),
('Ms. Congeniality'),
('Ghoulie'),
('Sherlock'),
('Bambino'),
('Foxy Lady'),
('MomBod'),
('Tater Tot'),
('Dino'),
('Little Bear'),
('Lil Girl'),
('Smarty'),
('Chickie'),
('Grease'),
('Muscles'),
('Weiner'),
('Donut'),
('Stud'),
('Chuckles'),
('Chef'),
('Gordo'),
('4-Wheel'),
('Queenie'),
('Friendo'),
('Ami'),
('Amour'),
('Chico'),
('Babs'),
('Bean'),
('Diet Coke'),
('Frogger'),
('Green Giant'),
('Turkey'),
('Terminator'),
('Tickles'),
('Dropout');


INSERT INTO Contact (people1,people2)
VALUES
    (48,32),
    (12,22),
    (18,10),
    (8,25),
    (26,37),
    (32,33),
    (5,40),
    (44,32),
    (1,13),
    (25,44);
INSERT INTO Contact (people1,people2)
VALUES
    (25,42),
    (29,25),
    (7,47),
    (16,47),
    (49,22),
    (30,27),
    (44,41),
    (38,40),
    (21,48),
    (33,16);
INSERT INTO Contact (people1,people2)
VALUES
    (28,45),
    (38,28),
    (38,49),
    (17,25),
    (48,39),
    (44,24),
    (3,28),
    (40,28),
    (27,11),
    (15,10);
INSERT INTO Contact (people1,people2)
VALUES
    (34,6),
    (16,11),
    (11,3),
    (4,4),
    (21,26),
    (6,41),
    (29,7),
    (20,44),
    (27,12),
    (15,45);
INSERT INTO Contact (people1,people2)
VALUES
    (43,5),
    (36,44),
    (25,40),
    (39,33),
    (36,8),
    (17,41),
    (1,28),
    (37,48),
    (47,48),
    (36,2);
INSERT INTO Contact (people1,people2)
VALUES
    (45,22),
    (33,23),
    (18,19),
    (13,44),
    (27,14),
    (29,33),
    (41,27),
    (33,20),
    (8,17),
    (32,44);
INSERT INTO Contact (people1,people2)
VALUES
    (47,47),
    (15,44),
    (12,28),
    (13,41),
    (49,15),
    (25,18),
    (24,40),
    (33,37),
    (35,9),
    (49,15);
INSERT INTO Contact (people1,people2)
VALUES
    (41,25),
    (40,8),
    (17,16),
    (22,31),
    (42,35);