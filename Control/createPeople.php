<link rel="stylesheet" href="../Views/style.css">

<?php 
    include_once "../Model/data.php";
    include_once "../Model/debug.php";

    $name = $_POST["name"];



// Vérifier si le champ name est vide
if(empty($_POST['name'])) { ?>
    <p class="colorRed">Erreur 400</p>
  
<?php }  else {
  // Vérifier si le champ name ne dépasse pas 200 caractères
  if (strlen($_POST['name']) >= 200) { ?>
    <p class="colorRed">Erreur 400</p>
    <?php http_response_code(400); } else {
    // Supprimer tout code HTML, JS ou PHP du champ name
    $name = strip_tags($_POST['name']);
    createPeople($name); 
    echo $name; 
    ?> <p class="ok">Tout est OK</p>
    <?php
    
  }
}

?>
<a href="../Views/index.php">index</a>