<link rel="stylesheet" href="style.css">
<div id="mynetwork"></div>
<script src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>
<script>
  fetch('../Control/fichier.json')
    .then(response => response.json())
    .then(data => {
      var nodes = new vis.DataSet(data.people);
      var edges = new vis.DataSet(data.contact);
      var container = document.getElementById("mynetwork");
      var data = { nodes: nodes, edges: edges };
      var options = {};
      var network = new vis.Network(container, data, options);
      console.log(data);
      console.log(nodes);
      console.log(edges);
    })
    .catch(error => {
      console.log(error);
    });
</script>
