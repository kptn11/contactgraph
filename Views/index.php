<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Contact Graph</title>
</head>
<body>

<?php
    include "../Model/data.php";
    include "../Views/Partials/menu.php";
    include '../Views/Partials/ajouter-personne.php';
    include '../Views/Partials/ajouter-contact.php';
    include '../Views/Partials/viewPeople.php';
    include '../Views/Partials/delete.php';
?>

    <script src="../src/script.mjs"></script>
</body>
</html>