
import fs from 'fs'


const CreerJson = document.getElementById('create-contact-BTN');
CreerJson.addEventListener('click', createJSON);

function createJSON() {
  var mysql = require('mysql');
  var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'tousStopAntiCovid',
    password: 'admin1234',
    database: 'ContactGraph'
  });

  connection.connect((err) => {
    if (err) throw err;
    console.log('Connected!');
    connection.query('SELECT * FROM people', (err, result) => {
      if (err) throw err;
      const people = result.map(({ id, name }) => ({ id, name }));
      connection.query('SELECT * FROM contact', (err, result) => {
        if (err) throw err;
        const contact = result.map(({ from, to }) => ({ from, to }));
        const data = JSON.stringify({ people, contact });
        fs.writeFile('contacts.json', data, (err) => {
          if (err) throw err;
          console.log('Data written to file');
        });
      });
    });
  });
}